#include <stdio.h>
#include "libs/truth_tables.h"
#include "libs/binary_operations.h"
#include "libs/set.h"

void firstSentenceTruthTable(bool a, bool b, bool c, bool* truthTable) {
    truthTable[0] = difference(c, a);
    truthTable[1] = intersection(truthTable[0], b);
    truthTable[2] = difference(c, a);
    truthTable[3] = difference(truthTable[2], b);
    truthTable[4] = combining(truthTable[1], truthTable[3]);
    truthTable[5] = intersection(a, b);
    truthTable[6] = symmetricDifference(truthTable[4], truthTable[5]);
}

Set firstSentenceSet(Set a, Set b, Set c, Set universe) {
    Set intermediateActions[6];

    intermediateActions[0] = differenceSet(c, a);
    intermediateActions[1] = intersectionSet(intermediateActions[0], b);
    intermediateActions[2] = differenceSet(c, a);
    intermediateActions[3] = differenceSet(intermediateActions[2], b);
    intermediateActions[4] = unionSet(intermediateActions[1], intermediateActions[3]);
    intermediateActions[5] = intersectionSet(a, b);

    return symmetricDifferenceSet(intermediateActions[4], intermediateActions[5]);
}

void secondSentenceTruthTable(bool a, bool b, bool c, bool* truthTable) {
    truthTable[0] = difference(a, b);
    truthTable[1] = symmetricDifference(truthTable[0], b);
    truthTable[2] = difference(truthTable[1], c);
}

Set secondSentenceSet(Set a, Set b, Set c, Set universe) {
    Set intermediateActions[2];

    intermediateActions[0] = differenceSet(a, b);
    intermediateActions[1] = symmetricDifferenceSet(intermediateActions[0], b);

    return differenceSet(intermediateActions[1], c);
}

void thirdSentenceTruthTable(bool a, bool b, bool c, bool* truthTable) {
    truthTable[0] = difference(a, c);
    truthTable[1] = difference(c, a);
    truthTable[2] = combining(truthTable[1], b);
    truthTable[3] = intersection(truthTable[0], truthTable[2]);
}

Set thirdSentenceSet(Set a, Set b, Set c, Set universe) {
    Set intermediateActions[3];

    intermediateActions[0] = differenceSet(a, c);
    intermediateActions[1] = differenceSet(c, a);
    intermediateActions[2] = unionSet(intermediateActions[1], b);

    return intersectionSet(intermediateActions[0], intermediateActions[2]);
}

void fourthSentenceTruthTable(bool a, bool b, bool c, bool* truthTable) {
    truthTable[0] = difference(a, c);
    truthTable[1] = negation(a);
    truthTable[2] = negation(b);
    truthTable[3] = intersection(truthTable[1], truthTable[2]);
    truthTable[4] = intersection(truthTable[3], c);
    truthTable[5] = symmetricDifference(truthTable[0], truthTable[4]);
}

Set fourthSentenceSet(Set a, Set b, Set c, Set universe) {
    Set intermediateActions[5];

    intermediateActions[0] = differenceSet(a, c);
    intermediateActions[1] = additionSet(a, universe);
    intermediateActions[2] = additionSet(b, universe);
    intermediateActions[3] = intersectionSet(intermediateActions[1], intermediateActions[2]);
    intermediateActions[4] = intersectionSet(intermediateActions[3], c);

    return symmetricDifferenceSet(intermediateActions[0], intermediateActions[4]);
}

void fifthSentenceTruthTable(bool a, bool b, bool c, bool* truthTable) {
    truthTable[0] = combining(a, b);
    truthTable[1] = negation(a);
    truthTable[2] = combining(truthTable[1], b);
    truthTable[3] = negation(b);
    truthTable[4] = intersection(truthTable[2], truthTable[3]);
    truthTable[5] = negation(c);
    truthTable[6] = combining(truthTable[4], truthTable[5]);
    truthTable[7] = intersection(truthTable[0], truthTable[6]);
}

Set fifthSentenceSet(Set a, Set b, Set c, Set universe) {
    Set intermediateActions[7];

    intermediateActions[0] = unionSet(a, b);
    intermediateActions[1] = additionSet(a, universe);
    intermediateActions[2] = unionSet(intermediateActions[1], b);
    intermediateActions[3] = additionSet(b, universe);
    intermediateActions[4] = intersectionSet(intermediateActions[2], intermediateActions[3]);
    intermediateActions[5] = additionSet(c, universe);
    intermediateActions[6] = unionSet(intermediateActions[4], intermediateActions[5]);

    return intersectionSet(intermediateActions[0], intermediateActions[6]);
}

void equivalentTransformations() {
    int operationAmounts[] = {7, 3, 4, 6, 8};
    bool** truthTables[] = {
            getTruthTable(operationAmounts[0], firstSentenceTruthTable),
            getTruthTable(operationAmounts[1], secondSentenceTruthTable),
            getTruthTable(operationAmounts[2], thirdSentenceTruthTable),
            getTruthTable(operationAmounts[3], fourthSentenceTruthTable),
            getTruthTable(operationAmounts[4], fifthSentenceTruthTable)
    };

    printf("Method of equivalent transformations:\n");

    for (int i = 0; i < 5; i++) {
        bool** currentTruthTable = truthTables[i];
        int currentOperationAmount = operationAmounts[i];

        for (int j = i + 1; j < 5; j++) {
            if (isTruthTablesEqual(currentTruthTable, currentOperationAmount, truthTables[j], operationAmounts[j])) {
                printf("B%d and B%d are identical\n", i + 1, j + 1);
            }
        }
    }
}

void setTheoreticMethod() {
    Set a = SET_OF(((int[]) {1, 4, 5, 7}));
    Set b = SET_OF(((int[]) {2, 4, 6, 7}));
    Set c = SET_OF(((int[]) {3, 5, 6, 7}));
    Set universe = SET_OF(((int[]) {0, 1, 2, 3, 4, 5, 6, 7}));

    Set sentenceSets[] = {
            firstSentenceSet(a, b, c, universe),
            secondSentenceSet(a, b, c, universe),
            thirdSentenceSet(a, b, c, universe),
            fourthSentenceSet(a, b, c, universe),
            fifthSentenceSet(a, b, c, universe)
    };

    printf("Set-theoretic method:\n");

    for (int i = 0; i < 5; i++) {
        Set currentSet = sentenceSets[i];

        for (int j = i + 1; j < 5; j++) {
            if (areEquals(currentSet, sentenceSets[j])) {
                printf("B%d and B%d are identical\n", i + 1, j + 1);
            }
        }
    }
}

int main() {
    equivalentTransformations();
    printf("\n");
    setTheoreticMethod();
}