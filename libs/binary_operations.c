#include "binary_operations.h"

bool difference(bool left, bool right) {
    return left > right;
}

bool intersection(bool left, bool right) {
    return left && right;
}

bool combining(bool left, bool right) {
    return left || right;
}

bool symmetricDifference(bool left, bool right) {
    return left ^ right;
}

bool negation(bool value) {
    return !value;
}