#include "set.h"
#include <malloc.h>
#include <minmax.h>
#include <stdio.h>

static size_t nearestCapacity(size_t size) {
    size_t capacity = 1;

    while (capacity < size) {
        capacity <<= 1;
    }

    return capacity;
}

static void ensureCapacity(Set* set) {
    if (set->size == set->capacity) {
        set->capacity <<= 1;
        set->elements = realloc(set->elements, sizeof(int) * set->capacity);
    }
}

Set createSetWithElements(const int* elements, size_t size) {
    size_t capacity = nearestCapacity(size);
    int* setElements = malloc(sizeof(int) * capacity);

    for (int i = 0; i < size; ++i) {
        setElements[i] = elements[i];
    }

    return (Set) {setElements, capacity, size};
}

Set createSetWithCapacity(size_t capacity) {
    return (Set) {malloc(sizeof(int) * capacity), capacity, 0};
}

void addToSet(Set* set, int value) {
    ensureCapacity(set);

    set->elements[set->size++] = value;
}

bool contains(Set set, int value) {
    for (size_t i = 0; i < set.size; ++i) {
        if (set.elements[i] == value) {
            return true;
        }
    }
    return false;
}

Set unionSet(Set left, Set right) {
    Set result = createSetWithElements(left.elements, left.size);

    for (size_t i = 0; i < right.size; ++i) {
        int value = right.elements[i];

        if (!contains(result, value)) {
            addToSet(&result, value);
        }
    }

    return result;
}

Set intersectionSet(Set left, Set right) {
    Set result = createSetWithCapacity(1);
    size_t minSize = min(left.size, right.size);

    for (size_t i = 0; i < minSize; ++i) {
        int value = left.elements[i];

        if (contains(right, value)) {
            addToSet(&result, value);
        }
    }

    return result;
}

Set differenceSet(Set left, Set right) {
    Set result = createSetWithCapacity(1);

    for (size_t i = 0; i < left.size; ++i) {
        int value = left.elements[i];

        if (!contains(right, value)) {
            addToSet(&result, value);
        }
    }

    return result;
}

Set symmetricDifferenceSet(Set left, Set right) {
    Set result = createSetWithCapacity(1);

    for (size_t i = 0; i < left.size; ++i) {
        int value = left.elements[i];

        if (!contains(right, value)) {
            addToSet(&result, value);
        }
    }

    for (size_t i = 0; i < right.size; ++i) {
        int value = right.elements[i];

        if (!contains(left, value) && !contains(result, value)) {
            addToSet(&result, value);
        }
    }

    return result;
}

Set additionSet(Set set, Set universe) {
    return differenceSet(universe, set);
}

bool areEquals(Set left, Set right) {
    if (left.size != right.size) {
        return false;
    }

    for (size_t i = 0; i < left.size; ++i) {
        if (!contains(right, left.elements[i])) {
            return false;
        }
    }

    return true;
}

void printSet(Set set) {
    printf("{");

    for (size_t i = 0; i < set.size; ++i) {
        printf("%d", set.elements[i]);

        if (i != (set.size - 1)) {
            printf(", ");
        }
    }

    printf("}\n");
}