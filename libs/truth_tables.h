#ifndef DISMATCH_1_3_TRUTH_TABLES_H
#define DISMATCH_1_3_TRUTH_TABLES_H

#define TRIO_TRUTH_TABLE_SIZE 8

#include <stdbool.h>

bool** getTruthTable(int operationAmount, void (*function)(bool, bool, bool, bool*));

void printTruthTable(bool** truthTable, int operationAmount);

bool isTruthTablesEqual(bool** left, int leftOperationAmount, bool** right, int rightOperationAmount);

#endif //DISMATCH_1_3_TRUTH_TABLES_H