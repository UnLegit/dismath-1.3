#include "truth_tables.h"
#include <malloc.h>
#include <stdio.h>

bool** getTruthTable(int operationAmount, void (*function)(bool, bool, bool, bool*)) {
    bool** truthTable = malloc(sizeof(bool*) * TRIO_TRUTH_TABLE_SIZE);

    int index = 0;

    for (int a = 0; a < 2; a++) {
        for (int b = 0; b < 2; b++) {
            for (int c = 0; c < 2; c++) {
                truthTable[index] = malloc(sizeof(bool) * operationAmount);

                function(a, b, c, truthTable[index++]);
            }
        }
    }

    return truthTable;
}

static void printSeparationLine(int dashAmount) {
    printf("|");

    for (int i = 0; i < dashAmount; ++i) {
        printf("-");
    }

    printf("|\n");

}

void printTruthTable(bool** truthTable, int operationAmount) {
    int dashAmount = (3 + operationAmount) * 4 - 1;

    printSeparationLine(dashAmount);

    printf("| A | B | C |");

    for (int j = 1; j <= operationAmount; ++j) {
        printf(" %d |", j);
    }

    printf("\n");
    printSeparationLine(dashAmount);

    int index = 0;

    for (int a = 0; a < 2; a++) {
        for (int b = 0; b < 2; b++) {
            for (int c = 0; c < 2; c++) {
                printf("| %d | %d | %d |", a, b, c);

                bool* truthRow = truthTable[index++];

                for (int j = 0; j < operationAmount; ++j) {
                    printf(" %d |", truthRow[j]);
                }

                printf("\n");
            }
        }
    }

    printSeparationLine(dashAmount);
}

bool isTruthTablesEqual(bool** left, int leftOperationAmount, bool** right, int rightOperationAmount) {
    int leftColumnIndex = leftOperationAmount - 1;
    int rightColumnIndex = rightOperationAmount - 1;

    for (int i = 0; i < TRIO_TRUTH_TABLE_SIZE; ++i) {
        if (left[i][leftColumnIndex] != right[i][rightColumnIndex]) {
            return false;
        }
    }

    return true;
}