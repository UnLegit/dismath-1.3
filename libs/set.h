#ifndef DISMATCH_1_3_SET_H
#define DISMATCH_1_3_SET_H

#include "memory.h"
#include <stdbool.h>

#define ARRAY_SIZE(array) sizeof(array) / sizeof(array[0])
#define SET_OF(array) createSetWithElements(array, ARRAY_SIZE(array))

typedef struct Set {
    int* elements;
    size_t capacity;
    size_t size;
} Set;

Set createSetWithElements(const int* elements, size_t size);

Set createSetWithCapacity(size_t capacity);

void addToSet(Set* set, int value);

bool contains(Set set, int value);

Set unionSet(Set left, Set right);

Set intersectionSet(Set left, Set right);

Set differenceSet(Set left, Set right);

Set symmetricDifferenceSet(Set left, Set right);

Set additionSet(Set set, Set universe);

bool areEquals(Set left, Set right);

void printSet(Set set);

#endif //DISMATCH_1_3_SET_H
