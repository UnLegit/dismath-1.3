#ifndef DISMATCH_1_3_BINARY_OPERATIONS_H
#define DISMATCH_1_3_BINARY_OPERATIONS_H

#include <stdbool.h>

bool difference(bool left, bool right);

bool intersection(bool left, bool right);

bool combining(bool left, bool right);

bool symmetricDifference(bool left, bool right);

bool negation(bool value);

#endif //DISMATCH_1_3_BINARY_OPERATIONS_H